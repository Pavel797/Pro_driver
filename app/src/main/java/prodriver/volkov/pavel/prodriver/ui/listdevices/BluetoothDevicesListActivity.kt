package prodriver.volkov.pavel.prodriver.ui.listdevices

import android.bluetooth.BluetoothAdapter
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import prodriver.volkov.pavel.prodriver.R

import kotlinx.android.synthetic.main.activity_bluetooth_devices_list.*
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Intent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import prodriver.volkov.pavel.prodriver.ui.joysticks.DriverActivity
import prodriver.volkov.pavel.prodriver.ui.joysticks.MAC_ADDRESS_KEY
import java.util.*
import kotlin.concurrent.timerTask

class BluetoothDevicesListActivity : AppCompatActivity() {

    private val REQUEST_ENABLE_BT = 1
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var myUUID: UUID? = null
    private var scannerDeviceTask: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth_devices_list)
        setTitle(R.string.select_device)
    }

    override fun onStart() {
        super.onStart()
        setupList()

        if (bluetoothAdapter != null && !bluetoothAdapter!!.isEnabled) {
            enableBluetooth()
        }
    }

    private fun enableBluetooth() {
        statusText.text = getString(R.string.bluetooth_not_enable)
        val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                setupList()
            } else {
                error(R.string.bluetooth_not_enable)
                enableBluetooth()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (scannerDeviceTask != null && scannerDeviceTask!!.isDisposed) {
            scannerDeviceTask!!.dispose()
        }
    }

    private fun setupList() {
        statusText.text = getString(R.string.scaning)

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH) ||
                bluetoothAdapter == null) {
            error(R.string.bluetooth_error)
            return
        }

        myUUID = UUID.fromString(getString(R.string.UUID))

        listDevices.layoutManager = LinearLayoutManager(this)
        listDevices.adapter = DeviceAdapter(object : DeviceAdapter.OnDeviceSelectListener {
            override fun onSelect(device: BluetoothDevice) {
                val intent = Intent(this@BluetoothDevicesListActivity, DriverActivity::class.java)
                intent.putExtra(MAC_ADDRESS_KEY, device.address)
                startActivity(intent)
            }
        })

        scannerDeviceTask = createScannerDevices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { setDevices ->
                    (listDevices.adapter as DeviceAdapter).clearAllAndAdd(setDevices)
                }
    }

    private fun createScannerDevices(): Observable<Set<BluetoothDevice>> {
        return Observable.create { subscriber ->
            val timer = Timer()
            timer.schedule(timerTask {
                if (!bluetoothAdapter!!.isEnabled) {
                    enableBluetooth()
                    timer.cancel()
                    subscriber.onComplete()
                }

                subscriber.onNext(bluetoothAdapter!!.bondedDevices)
            }, 0, 700)

            subscriber.setCancellable { timer.cancel() }
        }
    }

    private fun error(idStringMsg: Int) {
        statusText.text = getString(idStringMsg)
        Toast.makeText(this, idStringMsg, Toast.LENGTH_LONG).show()
    }
}
