package prodriver.volkov.pavel.prodriver.ui.listdevices

import android.bluetooth.BluetoothClass
import android.bluetooth.BluetoothDevice
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.blutooth_device_layout.*
import kotlinx.android.extensions.LayoutContainer

class ItemHolderBluetoothDevice(itemView: View, var selectListener: DeviceAdapter.OnDeviceSelectListener)
    : RecyclerView.ViewHolder(itemView), LayoutContainer {

    override val containerView: View = itemView

    fun bind(device: BluetoothDevice) {
        deviceName.text = device.name
        deviceType.text = getBTMajorDeviceClass(device.bluetoothClass.majorDeviceClass)
        deviceAddress.text = device.address
        connectButton.setOnClickListener { selectListener.onSelect(device) }
    }
}

private fun getBTMajorDeviceClass(major: Int): String {
    return when (major) {
        BluetoothClass.Device.Major.AUDIO_VIDEO -> "AUDIO_VIDEO"
        BluetoothClass.Device.Major.COMPUTER -> "COMPUTER"
        BluetoothClass.Device.Major.HEALTH -> "HEALTH"
        BluetoothClass.Device.Major.IMAGING -> "IMAGING"
        BluetoothClass.Device.Major.MISC -> "MISC"
        BluetoothClass.Device.Major.NETWORKING -> "NETWORKING"
        BluetoothClass.Device.Major.PERIPHERAL -> "PERIPHERAL"
        BluetoothClass.Device.Major.PHONE -> "PHONE"
        BluetoothClass.Device.Major.TOY -> "TOY"
        BluetoothClass.Device.Major.UNCATEGORIZED -> "UNCATEGORIZED"
        BluetoothClass.Device.Major.WEARABLE -> "AUDIO_VIDEO"
        else -> "unknown!"
    }
}