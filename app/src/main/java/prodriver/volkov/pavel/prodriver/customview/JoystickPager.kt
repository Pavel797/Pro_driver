package prodriver.volkov.pavel.prodriver.customview

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class JoystickPager : ViewPager {

    var isEnableSwipe: Boolean = true
        set
        get

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return isEnableSwipe
    }

    override fun canScroll(v: View?, checkV: Boolean, dx: Int, x: Int, y: Int): Boolean {
        return if (isEnableSwipe) super.canScroll(v, checkV, dx, x, y) else isEnableSwipe
    }
}