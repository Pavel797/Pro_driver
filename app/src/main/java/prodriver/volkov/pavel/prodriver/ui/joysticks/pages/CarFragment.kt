package prodriver.volkov.pavel.prodriver.ui.joysticks.pages

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import kotlinx.android.synthetic.main.fragment_car.*
import prodriver.volkov.pavel.prodriver.R

interface ManageCar {
    fun turnLeftOrRight(d: Int)
    fun turnXY(x: Int, y: Int)
}

class CarFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car, container, false)
    }


    @SuppressLint("SetTextI18n")
    override fun onStart() {
        super.onStart()

        directionYJoystick.setOnCheckedChangeListener { compoundButton, check ->
            if (check && directionXJoystick.isChecked) {
                joystick.isEnabled = false
            } else if (!check && !directionXJoystick.isChecked) {
                joystick.isEnabled = true
                joystick.buttonDirection = 0
            } else if (check) {
                joystick.isEnabled = true
                joystick.buttonDirection = 1
            } else {
                joystick.isEnabled = true
            }
        }

        directionXJoystick.setOnCheckedChangeListener { compoundButton, check ->
            if (check && directionYJoystick.isChecked) {
                joystick.isEnabled = false
            } else if (!check && !directionXJoystick.isChecked) {
                joystick.isEnabled = true
                joystick.buttonDirection = 0
            } else if (check) {
                joystick.isEnabled = true
                joystick.buttonDirection = -1
            } else {
                joystick.isEnabled = true
            }
        }

        val thisActivity: Activity?  = activity

        leftButton.setOnClickListener {
            if (thisActivity is ManageCar) {
                thisActivity.turnLeftOrRight(-100)
            }
        }

        rightButton.setOnClickListener {
            if (thisActivity is ManageCar) {
                thisActivity.turnLeftOrRight(100)
            }
        }

        joystick.isAutoReCenterButton = fixedJoystick.isChecked
        fixedJoystick.setOnCheckedChangeListener { _, check -> joystick.isAutoReCenterButton = check }

        joystick.setOnMoveListener({ angle, strength ->
            powerText.text = "${getString(R.string.power)} $strength"

            if (thisActivity is ManageCar) {
                thisActivity.turnXY((strength * Math.cos(Math.toDegrees(angle.toDouble()))).toInt(),
                        (strength * Math.sin(Math.toDegrees(angle.toDouble()))).toInt())
            }

        }, 200)
    }
}
