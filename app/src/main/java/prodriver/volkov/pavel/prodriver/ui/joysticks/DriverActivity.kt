package prodriver.volkov.pavel.prodriver.ui.joysticks

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_bluetooth_devices_list.*
import prodriver.volkov.pavel.prodriver.R

import kotlinx.android.synthetic.main.activity_driver.*
import prodriver.volkov.pavel.prodriver.ui.joysticks.adapter.JoystickAdapter
import java.io.IOException
import java.util.*
import android.widget.Toast
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import prodriver.volkov.pavel.prodriver.network.BluetoothDataManager
import prodriver.volkov.pavel.prodriver.ui.joysticks.pages.ManageCar

const val MAC_ADDRESS_KEY: String = "MAC_ADDRESS"


class DriverActivity : AppCompatActivity(), ManageCar {

    public override fun turnLeftOrRight(d: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    public override fun turnXY(x: Int, y: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val REQUEST_ENABLE_BT = 1

    private var myUUID: UUID? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var macAddressDevice: String? = null
    private var dataManager: BluetoothDataManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver)

        macAddressDevice = intent.getStringExtra(MAC_ADDRESS_KEY)
                ?: throw ExceptionInInitializerError("MAC address device not be empty")

        pager.isEnableSwipe = false
        pager.adapter = JoystickAdapter(supportFragmentManager)
    }

    override fun onStart() {
        super.onStart()
        if (bluetoothAdapter != null && !bluetoothAdapter!!.isEnabled) {
            enableBluetooth()
        }

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH) ||
                bluetoothAdapter == null) {
            error(R.string.bluetooth_error)
        } else {
            myUUID = UUID.fromString(getString(R.string.UUID))

            createConnectorToDevice(bluetoothAdapter!!.getRemoteDevice(macAddressDevice)).subscribe(object : Observer<BluetoothSocket> {
                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(socket: BluetoothSocket) {
                    dataManager = BluetoothDataManager(socket)
                }

                override fun onError(e: Throwable) {
                    Toast.makeText(this@DriverActivity, R.string.bad_connect, Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            })
        }
    }

    private fun createConnectorToDevice(device: BluetoothDevice): Observable<BluetoothSocket> {
        return Observable.fromCallable {
            var bluetoothSocket: BluetoothSocket? = null
            try {
                bluetoothSocket = device.createRfcommSocketToServiceRecord(myUUID)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            try {
                bluetoothSocket!!.connect()
            } catch (e: IOException) {
                e.printStackTrace()

                try {
                    bluetoothSocket!!.close()
                } catch (e1: IOException) {
                    e.printStackTrace()
                }
            }

            bluetoothSocket
        }
    }

    private fun enableBluetooth() {
        statusText.text = getString(R.string.bluetooth_not_enable)
        val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
    }
}
