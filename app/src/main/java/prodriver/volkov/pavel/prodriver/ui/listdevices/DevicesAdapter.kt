package prodriver.volkov.pavel.prodriver.ui.listdevices

import android.bluetooth.BluetoothDevice
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import prodriver.volkov.pavel.prodriver.R

class DeviceAdapter(private var selectListener: OnDeviceSelectListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnDeviceSelectListener {
        fun onSelect(device: BluetoothDevice)
    }

    private var items: MutableList<BluetoothDevice> = mutableListOf()

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemHolderBluetoothDevice(LayoutInflater.from(parent.context)
                .inflate(R.layout.blutooth_device_layout, parent, false), selectListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemHolderBluetoothDevice) {
            holder.bind(items[position])
        }
    }

    fun clearAllAndAdd(pairedDevices: Set<BluetoothDevice>) {
        items.clear()
        items.addAll(pairedDevices)
        notifyDataSetChanged()
    }
}