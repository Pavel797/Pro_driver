package prodriver.volkov.pavel.prodriver.network

import android.bluetooth.BluetoothSocket
import io.reactivex.Observable
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

class BluetoothDataManager(socket: BluetoothSocket) {

    private var connectedInputStream: InputStream = socket.inputStream
    private var connectedOutputStream: OutputStream = socket.outputStream

    private var bufferSize: Int = 10
        set
        get
    private var observableReadData: Observable<ByteArray> = createDataObservable(bufferSize)
        set
        get

    fun write(buffer: ByteArray) {
        connectedOutputStream.write(buffer)
    }

    private fun createDataObservable(bufferSize: Int = 10): Observable<ByteArray> {
        return Observable.create { subscriber ->
            val buffer = ByteArray(bufferSize)
            connectedInputStream.read(buffer)

            subscriber.onNext(buffer)
        }
    }
}