package prodriver.volkov.pavel.prodriver.ui.joysticks.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import prodriver.volkov.pavel.prodriver.ui.joysticks.pages.CarFragment
import prodriver.volkov.pavel.prodriver.ui.joysticks.pages.HandFragment

const val PAGE_COUNT = 2

class JoystickAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> CarFragment()
            1 -> HandFragment()
            else ->
                throw IndexOutOfBoundsException("Fragment with position = $position not found!")
        }
    }

    override fun getCount(): Int {
        return PAGE_COUNT
    }
}